﻿# Primeiros passos
## Maneiras de executar JS

- Usar navegador, no console ao clicar em F12
- Usar a ferramenta online [https://codepen.io/](https://codepen.io/)
- Salvar um arquivo com extensão ```.js``` e abrir com o navegador.
Utilizando o VSCode, digitar ```!``` e dar enter pro Emmet completar.
Criar a tag ```<script>``` dentro de ```<body>``` e adicionar a propriedade ```src``` apontando para o arquivo ```.js```.

```
<script src="./script.js"></script>
```

## Imprimindo na tela
```
console.log("Olá mundo!");
```

## Comentários
### Utilizar duas barras
```
// console.log("Este é um comentário")
```
### Utilizar comentário em bloco
```
/*
console.log("Este é um comentário")
*/
```

## Fim de linha
Pode-se utilizar o ponto e vírgula ```;``` no fim da linha, porém ele não é obrigatório.