﻿# Exercícios
## 01
Crie uma função que receba uma String em Celsius ou Fahrenheit e faça a transformação de uma unidade para outra.
Se a unidade passada estiver incorreta, mostrar uma mensagem.

C = (F - 32) * 5/9
F = C * 9/5 + 32

```
function transform(degree) {
  const celsiusExists = degree.toUpperCase().includes("C");
  const fahrenheitExists = degree.toUpperCase().includes("F");

  if (!celsiusExists && !fahrenheitExists) {
    throw new Error("Unidade não identificada");
  }

  // F -> C
  let updatedDegree = Number(degree.toUpperCase().replace("F", ""));
  let formula = (fahrenheit) => ((fahrenheit - 32) * 5) / 9;
  let degreeSign = "C";

  // C -> F
  if (celsiusExists) {
    updatedDegree = Number(degree.toUpperCase().replace("C", ""));
    formula = (celsius) => ((celsius * 9/5) + 32);
    degreeSign = "F";
  }

  return formula(updatedDegree) + degreeSign;
}

try {
  console.log(transform("10c"));
} catch (e) {
  console.log(e);
}
```

## 02
Crie um objeto com duas propriedades, ambas do tipo Array:
- receitas: []
- despesas: []

Crie uma função que irá calcular o total de receitas e despesas.
No fim irá mostrar uma mensagem se a família está com saldo positivo ou negativo, seguido do valor do saldo.

Solução:
```
let family = {
  receitas: [
    2500, 3200, 250.43, 360.45
  ],
  despesas: [
    320.34, 128.45, 176.87, 500.00
  ],
};

function soma(array) {
  let total = 0;
  
  /*
  for(let valor of array) {
    total += valor;
  };
  */
  
  array.forEach(valor => total += valor)

  return total;
}

function calcularSaldo() {
  const somaReceitas = soma(family.receitas)
  const somaDespesas = soma(family.despesas)

  const total = somaReceitas - somaDespesas;

  let textoSaldo = "negativo";

  if (total >= 0) {
    textoSaldo = "positivo";
  }

  console.log(`Seu saldo é ${textoSaldo}: R$ ${total.toFixed(2)}`)
}

calcularSaldo();
```

## 03
Dado o Array de Objetos de contas a pagar, imprima na tela um novo Array de objetos onde cada objeto terá um atributo a mais chamado ```valorFinal```.
A este novo atributo, atribuir:

- Se a conta estiver vencida, atribuir o valor + 10%
- Se a conta não estiver vencida, será o igual ao atributo ```valor```

O valor do atributo já deve estar formatado com duas cadas decimais e com vírgula ao invés de ponto.

```
const contas = [
    {
        loja: 'Loja C',
        valor: 340.45,
        vencida: true,
    },
    {
        loja: 'Loja A',
        valor: 745.254,
        vencida: false,
    },
    {
        loja: 'Loja B',
        valor: 150.879,
        vencida: true,
    },
    {
        loja: 'Loja D',
        valor: 650,
        vencida: false,
    },
]
```

A saída deverá ser:

```
{loja: "Loja C", valor: 340.45, vencida: true, valorFinal: "374,50"}
{loja: "Loja A", valor: 745.254, vencida: false, valorFinal: "745,25"}
{loja: "Loja B", valor: 150.879, vencida: true, valorFinal: "165,97"}
{loja: "Loja D", valor: 650, vencida: false, valorFinal: "650,00"}
```

Solução:

```
function format(number) {
    return number.toFixed(2).replace('.', ',')
}

const contas = [
    {
        loja: 'Loja C',
        valor: 340.45,
        vencida: true,
    },
    {
        loja: 'Loja A',
        valor: 745.254,
        vencida: false,
    },
    {
        loja: 'Loja B',
        valor: 150.879,
        vencida: true,
    },
    {
        loja: 'Loja D',
        valor: 650,
        vencida: false,
    },
]

const newContas = contas.map(c => ({
        ...c,
        valorFinal: c.vencida ? format(c.valor * 1.1) : format(c.valor)
    }
))

console.log(newContas)
```