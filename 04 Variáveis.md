﻿# Variáveis
## Temos 3 palavras reservadas para declaração de variáveis:
* var
```
var clima = "Quente"
```
* let
```
let clima = "Quente"
```
* const
```
const clima = "Quente"
```

## Não precisam ter um tipo definido, ou seja, não é tipada.
```
let clima = "Quente"
clima = true

console.log(typeof clima)
```

## Block statement
```
{
	// aqui dentro é um bloco
}
```

## Scope var
Utilizando a palavra reservada ```var```, criamos uma variável de escopo global (hoisting).
```
console.log('Existe X antes do bloco? ', x);

{
	var x = 0
}

console.log('Existe X depois do bloco? ', x)
```

## Scope let e const
São locais e só funcionam no escopo onde foram criadas.

Só podem ser acessadas após a declaração (não possui hoisting).

## Agrupando declarações
```
let age, isHuman

age = 30
isHuman = true
```

## Concatenando e interpolando variáveis

### Concatenando
```
let nome = "Felipe"

console.log("O nome é" + nome)
```

### Interpolando com template literals
```
let nome = "Felipe"

console.log(`O nome é ${nome}`)
```