﻿# Operadores
## New
```
let name = new String('João')
console.log(name)

name.surName = 'da Silva'

let number = new Number()
let date = new Date('2021-05-05')
```

## Unários
- Typeof
```
console.log(typeof "João")
```

- Delete

```
const person = {
  name: "João",
  age: 25,
}

delete person.name

console.log(person)
```

## Aritméticos
- Soma
  ```console.log(3.2 + 5)```
- Subtração
  ```console.log(22 - 5)```
- Multiplicação
  ```console.log(3.2 * 5)```
- Divisão
  ```console.log(25 / 5)```
- Resto da divisão
  ```console.log(25 % 5)```
- Incremento
  ```
  let number = 10;
  console.log(number++)
  console.log(++number)
  ```
- Decremento
  ```
  let number = 10;
  console.log(number--)
  console.log(--number)
  ```
- Exponencial
  ```console.log(3 ** 3)```

## Operadores de comparação
- **Igual a**
```
console.log(1 == 1)
console.log(2 == 1)
console.log(1 == "1")
```

- **Diferente de**
```
console.log(1 != 1)
console.log(2 != 1)
console.log(1 != "1")
```

- **Estritamente igual a**
Ele testa também o tipo de dado.
```
console.log(1 === 1)
console.log(1 === "1")
```

- **Estritamente diferente de**
Ele testa também o tipo de dado.
```
console.log(1 !== 1)
console.log(1 !== "1")
```

- **Maior que**
```
console.log(2 > 1)
```
- **Maior igual a**
```
console.log(2 >= 1)
```
- **Menor que**
```
console.log(2 < 1)
```
- **Menor igual a**
```
console.log(2 <= 1)
```

## Operadores de atribuição
- Adição
```
x += 2
```
- Subtração
```
x -= 2
```
- Multiplicação
```
x *= 2
```
- Divisão
```
x /= 2
```

## Operadores lógicos
- AND ```&&```
- OR ```||```
- NOT ```!```

## Condicional ternário
Sintaxe:
```
<condicao> ? <valor se verdadeiro> : <valor se falso>
```

Exemplo:
```
let pao = true
let queijo = true

const cafeDaManha = pao && queijo ? 'Café top!' : 'Café ruim'

console.log(cafeDaManha)
```