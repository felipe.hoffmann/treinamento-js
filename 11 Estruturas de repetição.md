﻿# Estruturas de repetição

## For
```
for (let i = 0; i < 10; i++ ) {

  if (i === 5) {
    continue
  }

  if (i === 8) {
    break
  }

  console.log(i)
}
```

## While
```
let i = 0

while(i < 10) {
  console.log(i)
  i++
}
```

## For of
```
let names = ['João', 'Paulo', 'Pedro']

for (let name of names) {
  console.log(name)
}
```

## For in
```
let person = {
  name: 'João',
  age: 25,
  height: 80,
}

for (let prop in person) {
  console.log(prop)
}
```