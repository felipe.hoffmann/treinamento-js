﻿# Manipulando dados
## Type conversion vs Type Coersion

```
console.log('9' + 5); // Type conversion
console.log(Number(9) + 5); // Type coersion
```
## String em números

```
let string = "123";
console.log(Number(string));

let number = 321;
console.log(String(number));
```

## Contando caracteres e dígitos

```
let word = "Paralelepipedo"
console.log(word.length)

let number = 1234
// console.log(number.length) - Não funciona
console.log(String(number).length)
```

## Casas decimais
Transformar um número com duas casas decimais e trocar ponto por vírgula:
```
let number = 342.234234
console.log(number.toFixed(2).replace(".", ","))
```

## Transformando maiúsculas e minúsculas
```
let word = "Programar é muito bacana!"
console.log(word.toLowerCase())
console.log(word.toUpperCase())
```

## Separando Strings
Separe um texto que contém espaços em um novo Array, onde cada palavra é uma posição no Array.
Depois disso, transforme o Array em um texto. E onde eram espaços, coloque _.

```
let phrase = "Programar é muito bacana!"
let myArray = phrase.split(" ")
let phraseWithUnderscore = myArray.join("_")

console.log(phraseWithUnderscore)
```

## Encontrando palavras em frases
```
let phrase = "Programar é muito bacana!"

console.log(phrase.includes("bacana"))
```