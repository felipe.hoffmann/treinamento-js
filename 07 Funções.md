﻿# Funções
## Declarando uma função
```
function createPhrases() {
	console.log('Estudar é muito bom)
	console.log('Paciência e persistência)
	console.log('Revisão é mãe do aprendizado)
}
```
## Executando uma função
```
createPhrases()
```

## Argumentos e parâmetros
```
function sum(n1, n2) {
	console.log(n1 + n2)
}

sum(2,3)
```

## Retornando valores
```
function sum(n1, n2) {
	return(n1 + n2)
}

console.log(sum(2,3))
```

## Arrow functions
Servem para criar funções anômias de forma curta.
```
const sayMyName = () => {
	console.log('Felipe')
}

sayMyName()
```
No código acima, a função foi atribuída à uma constante. Isso pode ser útil quando quisermos definir a função apenas onde precisamos dela. Em alguns momentos, pode tornar nosso código mais simples de entender.

## Callback function
Nada mais é do que passar uma função como argumento para outra função
```
function sayMyName(printName) {
	console.log('Antes de executar a função')

	printName()

	console.log('Depois de executar a função)
}

sayMyName(
	() => {
		console.log('Felipe')
	}
)
```

## Funções construtoras
Serve como molde para criação de objetos.
Muito útil para criação de novos atributos comuns à todos os objetos.
Uma boa prática é sempre criar a função começando o seu nome em letra maiúscula.
```
function Person(name) {
	this.name = name
	this.walk = () => {
		return this.name + " está andando."
	}
}

const felipe = new Person('Felipe')
const joao = new Person('João')

console.log(felipe.walk())
console.log(joao.walk())
```

Dentro do javascript temos diversas funções construtoras
```
const date = new Date('2021-01-01')

console.log(date)
```