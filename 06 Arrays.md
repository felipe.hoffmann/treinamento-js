﻿# Arrays
```
const animals = [
	'Lion',
	'Monkey',
	'Cat'
]
```

O Array pode conter elementos de vários tipos de dados misturados.

## Acessando valores dentro de um Array
```
console.log(animals[0])
```

## Obtendo o tamanho do Array
```
console.log(animals.length)
```

## Criando um Array com construtor

```
let myArray = new Array('Lion',	'Monkey',	'Cat')

let myArray2 = new Array(10)

console.log(myArray)
console.log(myArray2)
```

## Contar elementos de um Array
```
console.log(["a", "b", "c"].length)
```

## Transformar uma String em elementos de um Array
```
let word = "meu array"

console.log(Array.form(word))
```

## Manipulando Arrays
```
let techs = ["html", "css", "js"]
```

### Adicionar um item no fim
```
techs.push("Java")
```

### Adicionar um item no começo
```
techs.unshift("sql")
```

### Remover um item do fim
```
techs.pop()
```

### Remover um item do começo
```
techs.shift()
```

### Pegar somente alguns elementos do Array
```
console.log(techs.slice(1, 3))
```

### Remover um ou mais itens em qualquer posição do Array
```
techs.splice(1, 1)

console.log(techs)
```

### Encontrar a posição de um elemento no Array
```
let index = techs.indexOf("css")

console.log(index)
```

#### forEach
Recebe uma função como argumento. Itera sobre cada elemento do Array.
Não retorna nada.

```
let names = ['João','Pedro','Maria','José']

names.forEach(name => console.log(name))
```

#### map
Recebe uma função como argumento. Itera sobre cada elementro do array, mas diferente do ```forEach```, ele retorna um outro array.

```
let names = ['João','Pedro','Maria','José']

/*
function iteraArray(i) {
    return i;
}

let newArray = names.map(iteraArray(i))
*/

let newArray = name.map(i => i)


console.log(newArray)
```

### Filtrar elementos de um array

```
let x = [1, 2, 3, 4, 5, 6]

let y = x.filter(i => i > 3)

console.log(y)
```

## Rest e Spread operator
### Rest

```
function listagem(primeiro, segundo, ...outros) {
	console.log(primeiro)
	console.log(segundo)

	/*
	for (outro of outros) {
		console.log(outro)
	}
	*/

	outros.forEach(o => console.log(o))

}

listagem('João','Pedro','Maria','José')
```

### Spread

```
let x = [1, 2, 3]
let y = [...x]

y.push(8)

console.log(x, y)
```

Nota: Também pode ser utilizado em objetos.

## Referência
```https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array```