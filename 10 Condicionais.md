﻿# Condicionais e controle de fluxo

## if, else
```
if (true) {
  console.log('Verdadeiro')
} else {
  console.log('Falso')
}
```

## Switch
```
const number = 1;

switch (number) {
  case 1:
    console.log('O número é 1')
    break;
  case 2:
    console.log('O número é 2')
    break;
  default:
    console.log('Outro número')
    break;
}
```

## Throw e Try/Catch
```
function sayMyName(name) {
  if (name === '') {
    throw new Error("Nome é obrigatório");
  }

  console.log('Depois do erro')
}

try {

  sayMyName('')

} catch (e) {
  console.log(e)
}

```