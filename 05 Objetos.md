﻿# Objetos
```
const person {
	name: 'John',
	age: 30,
	weight: 88.6,
	isAdmin: true,
}
```

## Imprimindo o objeto
```
console.log(person)
```

## Imprimindo uma propriedade
### Utilizando um ponto "```.```"
```
console.log(person.name)
```

### Utilizando desestruturação
```
const { name } = person

console.log(name)
```







Nullish coalesce operator
Operações com objetos