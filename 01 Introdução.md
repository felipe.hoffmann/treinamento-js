﻿# Introdução
## O que é Javascript?
Linguagem de programação que roda no navegador do usuário.
Também roda em backend. Ex.: NodeJS

Podemos criar aplicações Web, Mobile (React Native), Desktop (Electron).

Empresas famosas que utilizam:
- Facebook
- Google
- Uber
- Netflix

## O que vamos ver?
- Tipos de dados
- Variáveis
- Objetos
- Funções
- Arrays
- Condicionais
- Estruturas de repetição