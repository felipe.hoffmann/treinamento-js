﻿# Tipos de dados

## String
* Aspas duplas ```""```
 ```
 console.log("Olá mundo!")
 ```
* Aspas simples ```''```
 ```
 console.log('Olá mundo!')
 ```
* Template literals ``` `` ```
 ```
 console.log(`Olá mundo!`)
 ```

## Number
* 33 - Inteiros
* 12.5 - reais
* NaN - Not a number:
 ```
 console.log(12.5 / "asdf")
 ```

## Boolean
* true
* false

## Undefined vs null
* undefined é indefinido

* null
	* Nulo
	* Objeto que não possui nada dentro. Existe mas não possui nada dentro.
	* Diferente de indefinido

```
console.log(null === undefined)
```

## Object
* Possui propriedades/atributos, funcionalidades/métodos
```
{ propriedade: "valor" }
```
```
console.log({
	nome: "Felipe",
	idade: 30,
	andar: function() {
		console.log("andar")
	}
	inativo: false,
})
```
## Array
```
console.log([
	"felipe",
	36
])
```